Rails.application.routes.draw do
  resources :posts do
    collection do
      get :search
    end
    resources :comments, only: [:create, :destroy]
  end
  
  resources :users do
    resources :profiles, only: [:new, :create, :show] do
      member do
        get :following, :followers
      end
    end
  end

  devise_for :users, path: "accounts", controllers: {
    registrations: 'users/registrations'
  }
  
  resources :relationships, only: [:create, :destroy]

  get '/feed', to: 'profiles#feed', as: :feed, defaults: { format: 'atom' }
  get 'tags/:tag', to: 'posts#index', as: :tag
  #get 'search', to: 'posts#search', as: :search

  root to: 'posts#index'
end

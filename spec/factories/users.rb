FactoryGirl.define do
  factory :user do
    email 'test@example.com'
    password 'poderpoder'
    password_confirmation 'poderpoder'
  end

end

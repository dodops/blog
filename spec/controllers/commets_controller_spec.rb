require 'rails_helper'

RSpec.describe CommentsController, :type => :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:posta) { FactoryGirl.create(:post, user: user) }

  describe 'deleting a comment' do
    let(:comment) { FactoryGirl.create(:comment, post: posta) }

    it 'should respond with success' do
      xhr :delete, :destroy, id: comment.id, post_id: posta.id
      expect(response).to be_success
    end
  end

  describe "creating a comment" do

    it "should responde successfuly" do
      xhr :post, :create, comment: {body: "sd"},post_id: posta.id
      expect(response).to be_success
    end
  
  end
end
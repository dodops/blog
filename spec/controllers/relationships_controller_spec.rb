require 'rails_helper'

RSpec.describe RelationshipsController, :type => :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:other) { FactoryGirl.create(:user, email: "poderpoer@gmail.com") }

  describe "#destroy" do
    it "should unfollow a user throught Ajax" do 
      sign_in(user)   
      user.follow(other)
      relationship = user.active_relationships.find_by(followed_id: other.id)
      xhr :delete, :destroy, id: relationship.id
      expect(response).to be_success
    end
  end

  describe "#create" do
    it "should follow a user with Ajax" do
      sign_in(user)
      xhr :post, :create, followed_id: other.id
      expect(response).to be_success
    end
  end
end

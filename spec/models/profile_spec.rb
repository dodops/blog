require 'rails_helper'

RSpec.describe Profile, :type => :model do
  let(:user) { FactoryGirl.create(:user) }
  let(:profile) { FactoryGirl.build(:profile, user: user) }

  it "should be valid" do
    expect(profile).to be_valid
  end

  it "should require a user" do
    profile.user = nil
    expect(profile).to_not be_valid
  end
end

require 'rails_helper'

RSpec.describe User, :type => :model do
  it { is_expected.to have_many :posts }
  it { is_expected.to have_many :comments }
  it { is_expected.to have_many :active_relationships }
  it { is_expected.to have_one :profile }

  let(:douglas) { FactoryGirl.create(:user) }
  let(:felipe) { FactoryGirl.create(:user, email: "ex@com.br") }
  let(:post) { FactoryGirl.create(:post, user: felipe) }

  describe "#feed" do
    it "feed should include posts from a followed user" do
      douglas.follow(felipe)
      felipe.posts.each do |post| 
        expect(douglas.feed).to include(post)
      end
    end

    it "should include posts from self" do
      felipe.posts.each do |post|
        expect(felipe.feed).to include(post)
      end
    end

    it "should not include posts from an unfollewed user" do
      felipe.posts.each do |post|
        expect(douglas.feed).to_not include(post)
      end
    end
  end

  describe "#following?" do
    it "should return false if an user follow an other" do
      expect(douglas.following?(felipe)).to be_falsey
    end
  end
  
  describe "#follow" do
    it "a given user" do
      douglas.follow(felipe)
      expect(douglas.following?(felipe)).to be_truthy
    end
  end

  describe "#unfollow" do
    it "should unfollow a given user" do
      douglas.follow(felipe)
      douglas.unfollow(felipe)
      expect(douglas.following?(felipe)).to be_falsey
    end
  end
end

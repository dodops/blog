require 'rails_helper'

RSpec.describe Post, :type => :model do
  it { should belong_to :user }
  it { should have_many :comments}

  let(:user) { FactoryGirl.create(:user)}
  let(:user2) { FactoryGirl.create(:user, email: "lo@xpto.org")}
  let(:post) { FactoryGirl.create(:post, user: user) }

  describe "#owned_by?" do
    it "true if a given user owns the post" do
      expect(post.owned_by?(user)).to be_truthy
    end

    it "false if a given user does not owns the post" do
      post.user = user2
      expect(post.owned_by?(user)).to be_falsey
    end
  end

  context "with valid params" do
    it { expect(post).to be_valid }
  end

  context "with invalid" do
    it "can't create without a user" do
      post.user = nil
      expect(post).to_not be_valid
    end
  end
end

require 'rails_helper'

RSpec.describe Comment, :type => :model do
  it { should belong_to :user }
  it { should belong_to :post }

  let(:user) { FactoryGirl.create(:user)}
  let(:post) { FactoryGirl.create(:post, user: user) }

  it "should create a new comment for a given post" do
    comment = post.comments.create(body: "teste")
    expect(comment).to be_valid
  end
end

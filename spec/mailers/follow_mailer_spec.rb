require "rails_helper"

RSpec.describe FollowMailer, :type => :mailer do
  let(:user) { FactoryGirl.create(:user) }
  let(:other) { FactoryGirl.create(:user, email: "other@mail.com") }
  let(:mail) { FollowMailer.send_message(user, other) }

  describe "#send_message" do
    it "should send a message to a user" do
      expect(mail.to).to eql([other.email])
    end

    it "should renders the subject" do
      expect(mail.subject).to eql('Hello bro, you get a new follower!!!')
    end
  end
end

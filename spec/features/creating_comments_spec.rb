require 'rails_helper'

feature "CreatingComments", :type => :feature do
  describe ".create", :js => true do
    let(:user) { FactoryGirl.create(:user, email: "kajubina@g.com")}
    let(:post) { FactoryGirl.create(:post, user: user) }

    it "append the js correctly" do
      login_user(user)
      

      visit post_path(post)
      fill_in "Body", with: "Dafug"
      click_button "Create comment"
      expect(page).to have_content("Dafug")
    end
  end
end

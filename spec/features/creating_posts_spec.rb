require 'rails_helper'

feature "CreatingPosts", :type => :feature do
  let(:user) { FactoryGirl.create(:user) }

  it "should create a news post" do
    login_user(user)
    visit '/'
    click_link "New Post"
    fill_in "Title", with: "PODER"
    fill_in "Body", with: "alkslkdlkdlksd"
    click_button "Post it!"
    expect(page).to have_content("Post has been created.")
  end
end

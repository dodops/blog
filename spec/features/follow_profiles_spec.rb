require 'rails_helper'

feature "FollowProfiles", :type => :feature do

  let(:user) { FactoryGirl.create(:user, email: "suffn@org.com") }
  let(:other) { FactoryGirl.create(:user, email: "mada@org.com") }
  let(:profile) { FactoryGirl.create(:profile, user: user) }
  let(:p2) { FactoryGirl.create(:profile, user: other) }

  it "shoud be able to get a list of following users" do
    login_user(user)

    visit user_profile_path(user, profile)
    click_link "following"
    expect(page).to have_content("users")
  end
  it "shoud be able to get a list of followers users" do
    login_user(user)
    visit user_profile_path(user, profile)
    click_link "followers"
    expect(page).to have_content("users")
  end
end

require 'rails_helper'

feature "CreatingProfiles", :type => :feature do
  context "after create a user" do

    it "should create a new profile" do

      visit '/accounts/sign_in'
      click_link "Sign up"
      fill_in "Email", with: "exmaple@ort.com"
      fill_in "Password", with: "poderpoder", match: :prefer_exact
      fill_in "Password confirmation", with: "poderpoder", match: :prefer_exact
      click_button "Sign up"

      fill_in "Name", with: "José Arcadio Buendia"
      fill_in "Address", with: "Macondo"
      fill_in "Bio", with: "kfodjfoiefoiewqn"

      click_button "Create profile"
      expect(page).to have_content("Profile created.")
      
    end
  end
end

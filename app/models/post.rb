class Post < ActiveRecord::Base
  belongs_to :user
  has_many :comments

  validates :user, :presence => true
  default_scope { order("created_at DESC") }

  delegate :email, to: :user, prefix: true

  acts_as_taggable

  searchable do
    text :title
  end

  def owned_by?(owner)
    user.id == owner.id
  end
end

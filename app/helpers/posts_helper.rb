module PostsHelper
  def show_tags(post)
    raw post.tag_list.map { |t| link_to t, tag_path(t) }.join(', ')
  end

  def owner?(post)
    post.user == current_user
  end
end

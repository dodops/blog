class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :search]

  respond_to :html

  def index
    if params[:tag]
      @posts = Post.tagged_with(params[:tag])
    else
      @posts = Post.all
    end
    respond_with(@posts)
  end

  def search
    @search = Post.search do
      fulltext params[:search]
    end
    @posts = @search.results
    render 'index'
  end

  def show
    @comment = @post.comments.build
    respond_with(@post)
  end

  def new
    @post = Post.new
    respond_with(@post)
  end

  def edit
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user
    flash[:notice] = "Post has been created." if @post.save
    respond_with(@post)
  end

  def update
    @post.update(post_params) if @post.owned_by?(current_user)
    respond_with(@post)
  end

  def destroy
    @post.destroy if @post.owned_by?(current_user)
    respond_with(@post)
  end

  private
    def set_post
      @post = Post.find(params[:id])
    end

    def post_params
      params.require(:post).permit(:title, :body, :tag_list)
    end
end

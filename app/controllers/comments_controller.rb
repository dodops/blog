class CommentsController < ApplicationController
  before_action :find_post


  def create
    @comment = @post.comments.build(comments_params)
    @comment.user = current_user
    if @comment.save
      respond_to do |wants|
        wants.html do
          flash[:notice] = "Commented created."
          redirect_to @post
        end
        wants.js
      end
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    respond_to do |format|
      format.html do
        flash[:success] = 'Comment deleted.'
        redirect_to @post
      end
      format.js
    end
  end

  private

  def find_post
    @post = Post.find(params[:post_id])
  end

  def comments_params
    params.require(:comment).permit(:body)
  end
end

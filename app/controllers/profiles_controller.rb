class ProfilesController < ApplicationController
  before_action :set_user, only: [:following, :followers, :show, :feed]
  before_action :authenticate_user!

  def following
    @users = @user.following
    render 'show_follow'
  end

  def followers
    @users = @user.followers
    render 'show_follow'
  end

  def feed
    @title = "Feed of #{@user.profile_name}"
    @posts = @user.posts
    @updated = @posts.first.updated_at unless @posts.empty?
    
    respond_to do |format|
      format.atom { render layout: false }
      format.rss { redirect_to feed_path(format: :atom), status: :moved_permanently }
    end
  end
  
  def create
    @profile = current_user.build_profile(profile_params)
    if @profile.save
      respond_to do |wants|
        wants.html do
          flash[:notice] = "Profile created."
          redirect_to posts_path
        end
      end
    end
  end

  def new
    @profile = current_user.build_profile
  end

  def show
    @profile = @user.profile
  end

  private

  def profile_params
    params.require(:profile).permit(:name, :address, :bio)
  end

  def set_user
    @user = User.find(params[:user_id])
  end
end

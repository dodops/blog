class RelationshipsController < ApplicationController

  def create
    @user = User.find(params[:followed_id])
    current_user.follow(@user)
    FollowMailer.send_message(current_user, @user).deliver_now
    respond_to do |f|
      f.html { redirect_to root_path }
      f.js
    end
  end

  def destroy
    @user = Relationship.find(params[:id]).followed
    current_user.unfollow(@user)
    respond_to do |f|
      f.html { redirect_to root_path }
      f.js
    end
  end

end

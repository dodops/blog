class FollowMailer < ApplicationMailer

  def send_message(user, followed)
    @user = user
    @followed = followed
    mail(to: @followed.email, subject: "Hello bro, you get a new follower!!!")
  end

end

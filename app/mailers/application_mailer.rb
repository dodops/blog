class ApplicationMailer < ActionMailer::Base
  default from: "dodop3000@gmail.com"
  layout 'mailer'
end
